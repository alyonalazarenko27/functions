const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  } else if (isNaN(parseFloat(str1)) || isNaN(parseFloat(str2))) {
    return false;
  }
  return (parseFloat(str1) + parseFloat(str2).toString());
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let count = 0;
  let commentCount = 0;
  listOfPosts.forEach(element => {
    if (element.author === authorName) {
      count++;
      element.comments.forEach(el => {
        if (el.comment) {
          commentCount++;
        }
      });
    }
  });
  
  return `Post: ${count}, comments: ${commentCount}`;
};

const tickets=(people)=> {
  let clerksMoney = 0;
  for (const sum of people) {
    if (clerksMoney >= sum - 25) {
      clerksMoney += 25;
    } else {
      return "NO";
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
